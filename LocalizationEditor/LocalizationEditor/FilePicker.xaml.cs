﻿using LocalizationEditor.Model;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Navigation;

namespace LocalizationEditor
{
    /// <summary>
    /// Interaction logic for FilePicker.xaml
    /// </summary>
    public partial class FilePicker : Page
    {
        public List<string> Files { get; set; }

        private FileHelper fileHelper = new FileHelper();

        public FilePicker()
        {
            InitializeComponent();
            InitPage();
            DataContext = this;
        }

        private void InitPage()
        {
            Files = fileHelper.GetAllFiles();
        }

        private void ListView_Click(object sender, RoutedEventArgs e)
        {
            var item = (sender as ListView).SelectedItem;
            if (item is string)
            {
                var tableView = new TableView(item as string);
                NavigationService navService = NavigationService.GetNavigationService(this);
                navService.Navigate(tableView);
            }
        }

        private void Add_Button_Click(object sender, RoutedEventArgs e)
        {
            var newFileName = NewFileName.Text;
            if (string.IsNullOrEmpty(newFileName))
            {
                return;
            }
            var locales = new JSONHelper().LoadLocales();
            foreach (var locale in locales.Items)
            {
                var path = @$"{Configuration.ProjectPath}\Assets\Resources\Locales\{locale.Key}\{newFileName}.json";
                if (!File.Exists(path))
                {
                    using (StreamWriter sw = File.CreateText(path))
                    {
                        var content = JsonConvert.SerializeObject(CreateEmptyLocalizationCollection(), Formatting.Indented);
                        sw.Write(content);
                    }
                }
                else
                {
                    MessageBox.Show($"File already exists!");
                    return;
                }
            }

            Files = fileHelper.GetAllFiles();
            FileListView.ItemsSource = Files;
        }

        private LocalizationCollection CreateEmptyLocalizationCollection()
        {
            return new LocalizationCollection
            {
                Items = new List<LocalizationItem>()
                {
                    new LocalizationItem
                    {
                        Key = string.Empty,
                        Value = string.Empty
                    }
                }
            };
        }
    }
}
