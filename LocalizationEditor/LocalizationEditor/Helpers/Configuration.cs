﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;

namespace LocalizationEditor
{
    class Configuration
    {
        /// <summary>
        /// Path to the project, must end with the folder name Project_ZERO, and no slash at the end.
        /// </summary>
        public static string ProjectPath = ConfigurationManager.AppSettings["ProjectPath"];
    }
}
