﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace LocalizationEditor
{
    class FileHelper
    {
        /// <summary>
        /// Reads all files from the first locale (all locales should have the same files)
        /// </summary>
        /// <returns>All translation files, without extension</returns>
        public List<string> GetAllFiles()
        {
            var jsonHelper = new JSONHelper();
            var firstLocaleKey = jsonHelper.LoadLocales().Items.First().Key;

            var path = @$"{Configuration.ProjectPath}\Assets\Resources\Locales\{firstLocaleKey}";

            DirectoryInfo directoryInfo = new DirectoryInfo(path);
            FileInfo[] files = directoryInfo.GetFiles("*.json");
            return files.Select(x => RemoveExtension(x.Name)).ToList();
        }

        /// <summary>
        /// Remove extension from file end.
        /// </summary>
        /// <param name="fileName">Name of the file, with exteinsion</param>
        /// <returns>File name without extension</returns>
        private string RemoveExtension(string fileName)
        {

            int fileExtPos = fileName.LastIndexOf(".");
            if (fileExtPos >= 0)
            {
                return fileName.Substring(0, fileExtPos);
            }
            return fileName;
        }
    }
}
