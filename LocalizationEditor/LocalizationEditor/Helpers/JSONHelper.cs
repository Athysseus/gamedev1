﻿using LocalizationEditor.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace LocalizationEditor
{
    class JSONHelper
    {
        /// <summary>
        /// Reads all the locales from locales.json file
        /// </summary>
        /// <returns>All locales</returns>
        public LocaleCollection LoadLocales()
        {
            var path = @$"{Configuration.ProjectPath}\Assets\Localization\locales.json";
            var jsonText = File.ReadAllText(path);
            return JsonConvert.DeserializeObject<LocaleCollection>(jsonText);
        }

        /// <summary>
        /// Reads the transalations from specific locale and filename
        /// </summary>
        /// <param name="locale">Language of the translation</param>
        /// <param name="fileName">File to save to</param>
        /// <returns>All translations from specific locale and fileName</returns>
        public LocalizationCollection LoadLocalizations(string locale, string fileName)
        {
            var path = @$"{Configuration.ProjectPath}\Assets\Resources\Locales\{locale}\{fileName}.json";
            return JsonConvert.DeserializeObject<LocalizationCollection>(File.ReadAllText(path));
        }

        /// <summary>
        /// Save translations for specific locale to specific fileName
        /// </summary>
        /// <param name="localizationCollection">Translations to save</param>
        /// <param name="fileName">File to save to</param>
        /// <param name="locale">Language of the translation</param>
        public void SaveLocalizations(LocalizationCollection localizationCollection, string fileName, string locale)
        {
            var path = @$"{Configuration.ProjectPath}\Assets\Resources\Locales\{locale}\{fileName}.json";
            File.WriteAllText(path, JsonConvert.SerializeObject(localizationCollection, Formatting.Indented));
        }
    }
}
