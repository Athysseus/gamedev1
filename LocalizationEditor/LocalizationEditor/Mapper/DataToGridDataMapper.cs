﻿using LocalizationEditor.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LocalizationEditor.Mapper
{
    class DataToGridDataMapper
    {
        /// <summary>
        /// Maps localization data for displayable data for DataGrid
        /// </summary>
        /// <param name="localizations">Localizations to map</param>
        /// <returns>Data for DataGrid to display</returns>
        public Dictionary<string, Dictionary<string, string>> ToTableData(Dictionary<string, LocalizationCollection> localizations)
        {
            var result = new Dictionary<string, Dictionary<string, string>>();
            foreach (var item in localizations.First().Value.Items)
            {
                result.Add(item.Key, new Dictionary<string, string>());
                foreach (var localization in localizations)
                {
                    result[item.Key].Add(localization.Key, localization.Value.Items.ToDictionary(x => x.Key, v => v.Value)[item.Key]);
                }
            }
            return result;
        }
    }
}
