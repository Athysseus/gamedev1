﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LocalizationEditor.Model
{
    class Locale
    {
        public string Key { get; set; }
        public string Name { get; set; }
    }
}
