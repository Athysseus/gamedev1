﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LocalizationEditor.Model
{
    class LocaleGridData
    {
        public string Key { get; set; }
        public Dictionary<string, string> Translations { get; set; }
    }
}
