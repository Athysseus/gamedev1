﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LocalizationEditor.Model
{
    class LocalizationCollection
    {
        public List<LocalizationItem> Items { get; set; }
    }
}
