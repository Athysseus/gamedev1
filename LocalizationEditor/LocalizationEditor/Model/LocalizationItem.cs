﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LocalizationEditor.Model
{
    class LocalizationItem
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
}
