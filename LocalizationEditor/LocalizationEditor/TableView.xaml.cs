﻿using LocalizationEditor.Mapper;
using LocalizationEditor.Model;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace LocalizationEditor
{
    /// <summary>
    /// Interaction logic for TableView.xaml
    /// </summary>
    public partial class TableView : Page
    {
        const string KEY = "Key";

        private readonly string fileName;
        private List<string> labels = new List<string>();
        private JSONHelper jsonHelper = new JSONHelper();
        private DataToGridDataMapper mapper = new DataToGridDataMapper();

        public TableView(string fileName)
        {
            this.fileName = fileName;
            InitializeComponent();
            LoadData();
        }

        private void LoadData()
        {
            var locales = jsonHelper.LoadLocales();
            var localizations = new Dictionary<string, LocalizationCollection>();
            LocaleDataGrid.ItemsSource = new List<dynamic>();
            AddColumnToGrid(KEY);
            foreach (var locale in locales.Items)
            {
                localizations.Add(locale.Key, jsonHelper.LoadLocalizations(locale.Key, fileName));
                AddColumnToGrid(locale.Key);
            }
            var tableData = mapper.ToTableData(localizations);
            foreach (var item in tableData)
            {
                AddRowToGrid(item.Key, item.Value);
            }

        }

        /// <summary>
        /// Adds new column to the DataGrid
        /// </summary>
        /// <param name="columnName">Name of the column</param>
        private void AddColumnToGrid(string columnName)
        {
            labels.Add(columnName);
            DataGridTextColumn textColumn = new DataGridTextColumn();
            textColumn.Header = columnName;
            textColumn.Binding = new Binding(columnName);

            // styles to enable multi line strings:
            Style elementStyle = (Style)LocaleDataGrid.Resources["TextElementStyle"];
            textColumn.ElementStyle = elementStyle;

            Style editTextStyle = (Style)LocaleDataGrid.Resources["EditTextStyle"];
            textColumn.EditingElementStyle = editTextStyle;

            LocaleDataGrid.Columns.Add(textColumn);
        }

        private void AddRowToGrid(string translationKey, Dictionary<string, string> values)
        {
            dynamic row = new ExpandoObject();

            ((IDictionary<string, object>)row)[labels[0]] = translationKey;
            for (int i = 1; i < labels.Count; i++)
            {
                ((IDictionary<string, object>)row)[labels[i]] = values[labels[i]];
            }
            var list = (IList<dynamic>)LocaleDataGrid.ItemsSource;

            list.Add(row);
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            var tableData = LocaleDataGrid.ItemsSource;
            var columns = LocaleDataGrid.Columns;
            foreach (var column in columns)
            {
                var localeKey = column.Header.ToString();
                if (localeKey != KEY)
                {
                    var localizationItems = new List<LocalizationItem>();
                    var validationSet = new HashSet<string>();
                    foreach (var row in tableData)
                    {
                        var expandoObject = (ExpandoObject)row;
                        var localizationKey = expandoObject.Where(v => v.Key == KEY).Select(x => x.Value).FirstOrDefault().ToString();
                        if (validationSet.Contains(localizationKey))
                        {
                            MessageBox.Show($"Duplicate key: {localizationKey}");
                            return;
                        }
                        validationSet.Add(localizationKey);
                        var value = expandoObject.Where(v => v.Key == localeKey).Select(x => x.Value).FirstOrDefault();
                        localizationItems.Add(
                            new LocalizationItem
                            {
                                Key = localizationKey,
                                Value = value.ToString()
                            });
                    }
                    var dataToSave = new LocalizationCollection { Items = localizationItems };
                    jsonHelper.SaveLocalizations(dataToSave, fileName, column.Header.ToString());
                }
            }
            MessageBox.Show("Save completed!");
        }
    }
}
