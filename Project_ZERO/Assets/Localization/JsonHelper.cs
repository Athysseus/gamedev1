﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

/// <summary>
/// As JsonUtility does not support root level collections, this helper class was created to wrap any object to an array.
/// Also supports dictionarys.
/// </summary>
public static class JsonHelper
{
    public static T[] FromJson<T>(string json)
    {
        Wrapper<T> wrapper = JsonUtility.FromJson<Wrapper<T>>(json);
        return wrapper.Items;
    }

    public static Dictionary<string, string> DictionaryFromJson<T>(string json)
    {
        var wrapper = FromJson<DictionaryItem>(json);
        return wrapper.ToDictionary(k => k.Key, v => v.Value);
    }

    /// <summary>
    /// Serialize array to json
    /// </summary>
    /// <typeparam name="T">Type of array items</typeparam>
    /// <param name="array">The array to serialize</param>
    /// <returns></returns>
    public static string ToJson<T>(T[] array)
    {
        Wrapper<T> wrapper = new Wrapper<T>();
        wrapper.Items = array;
        return JsonUtility.ToJson(wrapper);
    }

    #region Private helper classes

    [Serializable]
    private class Wrapper<T>
    {
        public T[] Items;
    }

    [Serializable]
    private class DictionaryWrapper<T>
    {
        public Dictionary<T, T> Items;
    }

    [Serializable]
    private class DictionaryItem
    {
        public string Key;
        public string Value;
    }

    #endregion
}


