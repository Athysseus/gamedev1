﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

[Serializable]
public class Locale
{
    public string Key;
    public string Name;

    public override string ToString()
    {
        return $"{Key} - {Name}";
    }
}

