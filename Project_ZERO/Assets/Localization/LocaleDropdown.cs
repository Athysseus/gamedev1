﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;

/// <summary>
/// Class to display the available locales with a TMP_Dropdown.
/// Also able to change current locale with this.
/// </summary>
class LocaleDropdown : MonoBehaviour
{
    public TMP_Dropdown dropdown;

    private List<Locale> locales;

    void Start()
    {
        locales = LocalizationManager.Instance.GetLocales();
        var options = new List<TMP_Dropdown.OptionData>();
        int selected = 0;
        for (int i = 0; i < locales.Count; ++i)
        {
            var locale = locales[i];
            options.Add(new TMP_Dropdown.OptionData(locale.Name));
            if (locale.Key.Equals(LocalizationManager.Instance.GetCurrentLocale()))
            {
                selected = i;
            }
        }
        dropdown.options = options;
        dropdown.value = selected;
        dropdown.onValueChanged.AddListener(LocaleSelected);
    }

    private void LocaleSelected(int index)
    {
        LocalizationManager.Instance.ChangeLocale(locales[index].Key);
    }
}

