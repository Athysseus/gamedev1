using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using System.Linq;

/// <summary>
/// Handles localization related tasks. Singleton.
/// </summary>
public class LocalizationManager : MonoBehaviour
{
    private const string LOCALE_PATH = "Locales";
    public static LocalizationManager Instance { get; private set; }
    public event EventHandler<string> OnLocaleChanged;

    /// <summary>
    /// The file which contains the available locales.
    /// </summary>
    public TextAsset localesJson;
    /// <summary>
    /// Determines which files should be read for localization.
    /// </summary>
    public List<string> filesToLoad;
    public string defaultLocale;

    private string currentLocale;
    private Locale[] locales;
    private Dictionary<string, StringLocalization> stringLocalizations;

    void Awake()
    {
        Instance = this;
        if (!string.IsNullOrEmpty(defaultLocale))
        {
            currentLocale = defaultLocale;
        }
        LoadLocales();
        LoadLocalizationStrings();
    }

    public List<Locale> GetLocales()
    {
        return locales.ToList();
    }

    public string GetCurrentLocale()
    {
        return currentLocale;
    }

    public string GetLocalizedString(string key)
    {
        return stringLocalizations[currentLocale].GetString(key);
    }

    public string GetLocalizedString(string key, params string[] parameters)
    {
        var localizedString = stringLocalizations[currentLocale].GetString(key);
        return string.Format(localizedString, parameters);
    }

    public void ChangeLocale(string locale)
    {
        currentLocale = locale;

        EventHandler<string> handler = OnLocaleChanged;
        if (handler != null)
        {
            handler(this, locale);
        }
    }

    /// <summary>
    /// Loads the locales from localesJson 
    /// </summary>
    private void LoadLocales()
    {
        locales = JsonHelper.FromJson<Locale>(localesJson.text);
    }

    /// <summary>
    /// Loads the localization strings.
    /// </summary>
    private void LoadLocalizationStrings()
    {
        stringLocalizations = new Dictionary<string, StringLocalization>();
        foreach (var locale in locales)
        {
            var localizations = new Dictionary<string, string>();
            foreach (var file in filesToLoad)
            {
                var resourcePath = $"{LOCALE_PATH}/{locale.Key}/{file}";
                var json = Resources.Load<TextAsset>(resourcePath);
                var localizationsInFile = JsonHelper.DictionaryFromJson<Dictionary<string, string>>(json.text);
                foreach (var localization in localizationsInFile)
                {
                    localizations.Add(localization.Key, localization.Value);
                }
            }
            stringLocalizations.Add(locale.Key, new StringLocalization(localizations));
        }
    }
}

