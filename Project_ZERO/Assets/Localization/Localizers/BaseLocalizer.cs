﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

/// <summary>
/// Base class for localizer objects.
/// </summary>
public abstract class BaseLocalizer : MonoBehaviour
{
    /// <summary>
    /// Called by base class when the locale changes.
    /// </summary>
    /// <param name="locale">The locale which was set.</param>
    protected abstract void ChangeLocalizedElement(string locale);

    /// <summary>
    /// Called in Unity's Start() Method, before subscribing to the locale change event.
    /// </summary>
    protected abstract void OnStart();

    protected void Start()
    {
        OnStart();
        SubscribeToLocalizedEvent();
    }

    private void SubscribeToLocalizedEvent()
    {
        LocalizationManager.Instance.OnLocaleChanged += LocalizationManager_OnLocaleChanged;
    }

    private void LocalizationManager_OnLocaleChanged(object sender, string locale)
    {
        ChangeLocalizedElement(locale);
    }


}

