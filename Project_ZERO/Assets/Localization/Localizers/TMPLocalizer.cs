using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

/// <summary>
/// Class to localize GameObjects with TextMeshPro
/// </summary>
public class TMPLocalizer : BaseLocalizer
{
    public string localizationKey;

    private TextMeshProUGUI textMeshProUGUI;

    protected override void OnStart()
    {
        textMeshProUGUI = gameObject.GetComponent<TextMeshProUGUI>();
        ChangeLocalizedElement(LocalizationManager.Instance.GetCurrentLocale());
    }

    protected override void ChangeLocalizedElement(string locale)
    {
        textMeshProUGUI.text = LocalizationManager.Instance.GetLocalizedString(localizationKey);
    }
}
