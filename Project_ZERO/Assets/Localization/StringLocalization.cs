﻿using System.Collections.Generic;

/// <summary>
/// Holds string localization data.
/// </summary>
class StringLocalization
{
    private Dictionary<string, string> localizedString;

    public StringLocalization(Dictionary<string, string> localizedString)
    {
        this.localizedString = localizedString;
    }

    public string GetString(string key)
    {
        return localizedString[key];
    }

}

