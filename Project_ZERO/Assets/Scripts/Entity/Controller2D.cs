﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller2D : RaycastController
{
    public CollisionInfo collInfo;
    public Vector2 playerInput;

    public bool canPush = true;

    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();
    }

    public Vector2 Move(Vector2 moveAmount, bool standingOnPlatform)
    {
        return Move(moveAmount, Vector2.zero, standingOnPlatform);
    }

    public Vector2 Move(Vector2 moveAmount, Vector2 input, bool standingOnPlatform = false)
    {
        UpdateRaycastOrigins();

        collInfo.Reset();
        collInfo.moveAmountOld = moveAmount;

        playerInput = input;

        if(moveAmount.x != 0)
        {
            collInfo.faceDir = (int)Mathf.Sign(moveAmount.x);
        }

        HorizontalCollisions(ref moveAmount);

        if(moveAmount.y != 0)
        {
            VerticalCollisions(ref moveAmount);
        }

        if(standingOnPlatform)
        {
            collInfo.below = true;
        }

        transform.Translate(moveAmount);

        return moveAmount;
    }

    private void HorizontalCollisions(ref Vector2 moveAmount)
    {
        float originalMoveAmountX = moveAmount.x;
        Collider2D otherCollider = null;

        float dirX = collInfo.faceDir;
        float moveXAbs = Mathf.Abs(moveAmount.x);
        float rayLength = (moveXAbs < skinWidth) ? 2 * skinWidth : moveXAbs + skinWidth;

        for(int i = 0; i < horRayCount; ++i)
        {
            Vector2 rayOrigin = (dirX == -1) ? rayOrigins.bottomLeft : rayOrigins.bottomRight;
            rayOrigin += Vector2.up * (horRaySpacing * i);
            RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.right * dirX, rayLength, collisionMask);

            Debug.DrawRay(rayOrigin, Vector2.right * dirX, Color.red);
            if(hit)
            {
                if (hit.distance == 0)
                    continue;

                otherCollider = hit.collider;
                moveAmount.x = (hit.distance - skinWidth) * dirX;
                rayLength = hit.distance;
                collInfo.left = dirX == -1;
                collInfo.right = dirX == 1;
            }
        }

        if (!canPush)
            return;

        if(otherCollider != null && otherCollider.CompareTag("Pushable"))
        {
            //Push object
            Vector2 pushAmount = otherCollider.gameObject.GetComponent<PushableObject>().Push(new Vector2(originalMoveAmountX, 0));
            moveAmount = new Vector2(pushAmount.x, moveAmount.y + pushAmount.y);
            collInfo.left = false;
            collInfo.right = false;
        }
    }

    private void VerticalCollisions(ref Vector2 moveAmount)
    {
        float dirY = Mathf.Sign(moveAmount.y);
        float rayLength = Mathf.Abs(moveAmount.y) + skinWidth;

        for(int i = 0; i < verRayCount; ++i)
        {
            Vector2 rayOrigin = (dirY == -1) ? rayOrigins.bottomLeft : rayOrigins.topLeft;
            rayOrigin += Vector2.right * (verRaySpacing * i + moveAmount.x);
            RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.up * dirY, rayLength, collisionMask);

            Debug.DrawRay(rayOrigin, Vector2.up * dirY * rayLength, Color.red);

            if(hit)
            {

                moveAmount.y = (hit.distance - skinWidth) * dirY;
                rayLength = hit.distance;

                collInfo.below = dirY == -1;
                collInfo.above = dirY == 1;
            }
        }
    }

    private void ResetFallingThroughPlatform()
    {
        collInfo.fallingTHroughPlatform = false;
    }

    public struct CollisionInfo
    {
        public bool above, below;
        public bool left, right;

        public Vector2 moveAmountOld;
        public int faceDir;
        public bool fallingTHroughPlatform;

        public void Reset()
        {
            above = below = false;
            left = right = false;
        }
    }
}
