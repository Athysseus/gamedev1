using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Controller2D))]
public class EntityController : MonoBehaviour
{
    #region Move Properties
    public float maxJumpHeight = 4.25f;
    public float timeToJumpApex = .3f;
    public float moveSpeed = 6.5f;
    public float moveSpeedInAir = 10;

    protected Vector2 dirInput;

    [SerializeField]
    protected int maxJump = 1;
    protected int jumpCount;
    #endregion

    #region Physics Properties
    protected float gravity;
    protected float jumpVelocity;
    protected Vector2 velocity;

    protected Controller2D controller;
    #endregion

    public void SetDirectionInput(Vector2 input)
    {
        dirInput = input;
        if (dirInput.x != 0)
        {
            transform.localScale = new Vector3(0.75f * dirInput.x, 1, 1);
        }
    }

    protected virtual void Awake()
    {
        controller = GetComponent<Controller2D>();
    }

    protected virtual void Start()
    {
        gravity = -(2 * maxJumpHeight) / Mathf.Pow(timeToJumpApex, 2);

        jumpVelocity = Mathf.Abs(gravity) * timeToJumpApex;
    }

    protected virtual void FixedUpdate()
    {
        CalculateVelocity();
        controller.Move(velocity * Time.fixedDeltaTime, dirInput);
        if (controller.collInfo.above || controller.collInfo.below)
        {
            velocity.y = 0;
            jumpCount = controller.collInfo.below ? 0 : jumpCount;
        }
    }

    protected virtual void CalculateVelocity()
    {
        velocity.x = dirInput.x * moveSpeed;
        velocity.y += gravity * Time.fixedDeltaTime;
    }
}
