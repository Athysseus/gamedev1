﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]
public class PlatformActivater : MonoBehaviour
{
    public LayerMask activateMask;
    public PlatformController platform;

    private BoxCollider2D boxColl;

    
    private const float skinWidth = .015f;
    const float dstBetweenRays = .25f;
    private int rayCount;
    private float raySpacing;

    private Vector2 rayOrigin;

    private void Awake()
    {
        boxColl = GetComponent<BoxCollider2D>();
    }

    private void Start()
    {
        Bounds bounds = boxColl.bounds;
        bounds.Expand(skinWidth * -2);

        float width = bounds.size.x;

        rayCount = Mathf.RoundToInt(width / dstBetweenRays);

        raySpacing = bounds.size.x / (rayCount - 1);

        rayOrigin = new Vector2(bounds.min.x, bounds.max.y);
        platform.active = false;
    }

    private void Update()
    {
        float rayLength = 0.25f;
        bool wasHit = false;
        for (int i = 0; i < rayCount; ++i)
        {
            Vector2 actOrigin = rayOrigin + (Vector2.right * i * raySpacing);
            RaycastHit2D hit = Physics2D.Raycast(actOrigin, Vector2.up, rayLength, activateMask);
            if(hit)
            {
                rayLength = hit.distance;
                wasHit = true;
            }
            Debug.DrawRay(actOrigin, Vector2.up * rayLength, Color.yellow);
        }
        platform.active = wasHit;
    }
}
