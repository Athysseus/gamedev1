﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Controller2D))]
public class Player : EntityController
{
    private float velocityXSmoothing;
    public float minJumpHeight = 2.25f;
    private float minJumpVelocity;

    [SerializeField]
    private float accTimeAirborne = .05f;
    [SerializeField]
    private float accTimeGrounded = .025f;

    public void OnJumpInputDown()
    {
        if (jumpCount < maxJump)
        {
            velocity.y = jumpVelocity;
            if (dirInput.x != 0)
            {
                velocity.x = 10 * dirInput.x;
            }
            ++jumpCount;
        }
    }

    public void OnJumpInputUp()
    {
        if (velocity.y > minJumpVelocity)
        {
            velocity.y = minJumpVelocity;
        }
    }

    protected override void Awake()
    {
        base.Awake();
    }

    protected override void Start()
    {
        base.Start();

        minJumpVelocity = Mathf.Sqrt(2 * Mathf.Abs(gravity) * minJumpHeight);
    }

    protected override void FixedUpdate()
    {
        base.FixedUpdate();
    }

    protected override void CalculateVelocity()
    {
        float speed = moveSpeed;
        float accTime = accTimeGrounded;
        if (!controller.collInfo.below)
        {
            speed = moveSpeedInAir;
            accTime = accTimeAirborne;
        }
        float targetX = dirInput.x * speed;
        velocity.x = Mathf.SmoothDamp(velocity.x, targetX, ref velocityXSmoothing, accTime);
        velocity.y += gravity * Time.fixedDeltaTime;
    }
}
