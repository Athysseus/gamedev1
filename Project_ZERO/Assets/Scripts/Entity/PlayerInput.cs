﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Player))] 
public class PlayerInput : MonoBehaviour
{
    Player player;

    private void Awake()
    {
        player = GetComponent<Player>();
    }

    private void Update()
    {
        Vector2 dirInput = new Vector2(Input.GetAxisRaw("Horizontal"), 0);
        player.SetDirectionInput(dirInput);
        //paraszt forgatás
        if(dirInput.x < 0)
        {
            transform.localScale = new Vector3(-1, 1, 1);
        }else if(dirInput.x > 0)
        {
            transform.localScale = new Vector3(1, 1, 1);
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            player.OnJumpInputDown();
        }

        if (Input.GetKeyUp(KeyCode.Space))
        {
            player.OnJumpInputUp();
        }
    }
}
