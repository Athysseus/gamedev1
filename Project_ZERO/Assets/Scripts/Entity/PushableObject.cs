﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Controller2D))]
public class PushableObject : MonoBehaviour
{
    public float gravity = 12;
    private Controller2D controller;
    private Vector3 velocity;


    // Start is called before the first frame update
    private void Awake()
    {
        controller = GetComponent<Controller2D>();
    }

    // Update is called once per frame
    private void Update()
    {
        velocity += Vector3.down * gravity * Time.deltaTime;
        controller.Move(velocity * Time.deltaTime, false);
        if (controller.collInfo.below)
        {
            velocity = Vector3.zero;
        }
    }

    public Vector2 Push(Vector2 amount)
    {
        return controller.Move(amount, false);
    }
}
