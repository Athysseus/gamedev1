﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]
public class RaycastController : MonoBehaviour
{
    public LayerMask collisionMask;
    protected const float skinWidth = .015f;

    public int horRayCount = 4;
    public int verRayCount = 4;

    protected float horRaySpacing;
    protected float verRaySpacing;

    public BoxCollider2D boxColl;
    protected RaycastOrigins rayOrigins;

    public virtual void Awake()
    {
        boxColl = GetComponent<BoxCollider2D>();
    }

    public virtual void Start()
    {
        CalculateRaySpacing();
    }

    protected void CalculateRaySpacing()
    {
        Bounds bounds = boxColl.bounds;
        bounds.Expand(skinWidth * -2);

        horRayCount = Mathf.Clamp(horRayCount, 2, int.MaxValue);
        verRayCount = Mathf.Clamp(verRayCount, 2, int.MaxValue);

        horRaySpacing = bounds.size.y / (horRayCount - 1);
        verRaySpacing = bounds.size.x / (verRayCount - 1);
    }

    protected void UpdateRaycastOrigins()
    {
        Bounds bounds = boxColl.bounds;
        bounds.Expand(skinWidth * -2);

        rayOrigins.bottomLeft = new Vector2(bounds.min.x, bounds.min.y);
        rayOrigins.bottomRight = new Vector2(bounds.max.x, bounds.min.y);
        rayOrigins.topLeft = new Vector2(bounds.min.x, bounds.max.y);
        rayOrigins.topRight = new Vector2(bounds.max.x, bounds.max.y);
    }

    public struct RaycastOrigins
    {
        public Vector2 topLeft, topRight;
        public Vector2 bottomLeft, bottomRight;
    }
}
